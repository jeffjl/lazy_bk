package com.linln.modules.system.repository;

import com.linln.modules.system.domain.Article;
import com.linln.modules.system.domain.Comment;
import com.linln.modules.system.repository.BaseRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author 小懒虫
 * @date 2020/01/02
 */
public interface CommentRepository extends BaseRepository<Comment, Long> {

    List<Comment> findAllByArticleAndStatus(Article article, Byte status);
    @Query(nativeQuery = true, value = "select count(1) from or_comment")
    int countComment();

    @Query(nativeQuery = true, value = "SELECT COUNT(1)FROM or_comment WHERE TO_DAYS( NOW( ) ) = TO_DAYS(create_date )")
    int countCommentNow();
}