package com.linln.modules.system.service.impl;

import com.linln.common.constant.ParamConst;
import com.linln.common.data.PageSort;
import com.linln.common.enums.StatusEnum;
import com.linln.modules.system.domain.Mail;
import com.linln.modules.system.repository.MailRepository;
import com.linln.modules.system.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Properties;



/**
 * @author 小懒虫
 * @date 2020/04/19
 */
@Service
public class MailServiceImpl implements MailService {

    @Autowired
    private MailRepository mailRepository;

    /**
     * 根据ID查询数据
     * @param id 主键ID
     */
    @Override
    @Transactional
    public Mail getById(Long id) {
        return mailRepository.findById(id).orElse(null);
    }

    /**
     * 获取分页列表数据
     * @param example 查询实例
     * @return 返回分页数据
     */
    @Override
    public Page<Mail> getPageList(Example<Mail> example) {
        // 创建分页对象
        PageRequest page = PageSort.pageRequest("messageNumber", Sort.Direction.DESC);
        return mailRepository.findAll(example, page);
    }

    /**
     * 保存数据
     * @param mail 实体对象
     */
    @Override
    public Mail save(Mail mail) {
        return mailRepository.save(mail);
    }

    /**
     * 状态(启用，冻结，删除)/批量状态处理
     */
    @Override
    @Transactional
    public Boolean updateStatus(StatusEnum statusEnum, List<Long> idList) {
        return mailRepository.updateStatus(statusEnum.getCode(), idList) > 0;
    }


}