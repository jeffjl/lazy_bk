package com.linln.common.websocket;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.linln.common.utils.SigarUtil;


import javax.websocket.Session;
import java.util.HashMap;
import java.util.Map;

public class SystemStatusThread extends Thread {

    private Session session;

    public SystemStatusThread(Session session) {

        this.session = session;

    }

    @Override
    public void run() {
        int i = 1;
        Map map =new HashMap();
        while (i ==1) {
            try {
                map.put("cpu", SigarUtil.cpu());
                map.put("mem", SigarUtil.memory().getUsedPercent()+"%");
                JSONObject itemJSONObj = JSONObject.parseObject(JSON.toJSONString(map));
                session.getBasicRemote().sendText(itemJSONObj.toJSONString());
            } catch (Exception e) {
                 i =0;
                e.printStackTrace();
            }
        }
    }

}
